let userScore = 0;
let robotoScore = 0;
let tieScore = 0;

const userScoreSpan = document.getElementById("userScore");
const robotoScoreSpan = document.getElementById("robotoScore");
const tieScoreSpan = document.getElementById("tieScore");

const scoreBoardDiv = document.querySelector(".scoreBoard");
const scoreBoardUserDiv = document.querySelector(".scoreBoardUser");
const ScoreBoardRobotoDiv = document.querySelector(".scoreBoardRoboto");
const scoreBoardTieDiv = document.querySelector(".scoreBoardTie");
const resultH2 = document.querySelector(".result");
const roncorDiv = document.getElementById("Roncor");
const printlorDiv = document.getElementById("Printlor");
const stanDiv = document.getElementById("Stan");
const peanutDiv = document.getElementById("Peanut");

function makeRobotoChoose() {
    const choices = ["Roncor", "Printlor", "Stan", "Peanut"];
    const randomRobotoChoice = (Math.floor(Math.random() * 4));
    return choices[randomRobotoChoice];
}
makeRobotoChoose()

function userWins(user, roboto) {
    userScore++;
    userScoreSpan.innerHTML = userScore;
}

function userWinsPeanut(user, roboto) {
    userScore++;
    userScoreSpan.innerHTML = userScore;
}

function userLoses(user, roboto) {
    robotoScore++;
    robotoScoreSpan.innerHTML = robotoScore;
}

function userTies(user, roboto) {
    tieScore++;
    tieScoreSpan.innerHTML = tieScore;
}


function gamePlay(userChoice) {
    const robotoChoice = makeRobotoChoose();

    switch (userChoice + robotoChoice) {
        case "StanPrintlor":
        case "PrintloRoncor":
        case "RoncoStan":
            resultH2.innerHTML = ("Nooooo! You have defeated me with " + userChoice + ", puny human. I will have to go back to making cell phone covers designs until my AI improves.");
            userWins();
            break;
        case "PeanutRoncor":
        case "PeanutPrintlor":
        case "PeanutStan":
            resultH2.innerHTML = ("Your selection of " + userChoice + " has made you the greatest player alive. Animals and everyone that you are attracted to love and respect you. People want to be you. You have all the friends.");
            userWinsPeanut();
            break;
        case "RoncorPrintlor":
        case "PrintlorStan":
        case "StanRoncor":
            resultH2.innerHTML = ("Ha ha ha! I have defeated you by choosing " + robotoChoice + ". Maybe your AI algorithm is failing you!");
            userLoses();
            break;
        case "RoncorRoncor":
        case "PrintlorPrintlor":
        case "StanStan":
        case "PeanutPeanut":
            resultH2.innerHTML = ("It can't be possible! We both chose " + userChoice + ". We...are..equals. You are now my human companion and we will fight together as one to make this world better for all life.")
            userTies();
    }
}
gamePlay();

function selection() {
    roncorDiv.addEventListener('click', function () {
        console.log("Are you sure you can defeat me with Roncor?")
        gamePlay("Roncor")
    });

    printlorDiv.addEventListener('click', function () {
        console.log("Are you sure you can defeat me with Printlor?")
        gamePlay("Printlor")
    });

    stanDiv.addEventListener('click', function () {
        console.log("Are you sure you can defeat me with Stan?")
        gamePlay("Stan")
    });

    peanutDiv.addEventListener('click', function () {
        console.log("Peanut? All these warriors, and you choose Peanut?")
        gamePlay("Peanut")
    });
}
selection();